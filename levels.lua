-- Caution !
-- The numeral indice of level and picture and if_wrong and if_good must be the same for each level!

-- Define levels here
--[[
-- Mandatory keys for each level: startpoint, basepoint
-- Optional keys for each level: endpoint, hp, privs, startcall, endcall, registercall, awards, awards_needed
--]]

gnlevels.base_levels = {

	level1 =  {
	startpoint = {x=37,y=6,z=14},   -- start point is an offset relative to basepoint
	basepoint = {x=596,y=-32, z=228},
	hp = 4,
	mode = "single",
	protection = "all",
	--infos = {"info1"}
	},

	level2 = {
	startpoint = {x=6,y=34,z=21},   -- start point is an offset relative to basepoint
	mode = "single",
	hp=4,
	basepoint = {x=658,y=-11,z=426},
	protection = "all",
	--infos = {"info1","info2"}
	},

	level3 = {
	startpoint = {x=10,y=9,z=17},   -- start point is an offset relative to basepoint
	basepoint = {x=55,y=6,z=277},
	hp=4,
	mode = "single",
	protection = "all",
	},

	level4 = {
	startpoint = {x=8,y=5,z=37},   -- start point is an offset relative to basepoint
	basepoint = {x=1607,y=100,z=1588},
	hp=4,
	mode = "single",
	protection = "all",
	hp = 1,
	},

	level5= {
	startpoint = {x=27,y=10,z=25},   -- start point is an offset relative to basepoint
	basepoint = {x=2000,y=10,z=2000},
	mode = "single",
	hp=8,
	protection = "all",
	},

	level6= {
	-- startpoint = {x=46,y=4,z=54},   -- start point is an offset relative to basepoint
	startpoint = {x=52,y=9,z=52},   -- start point is an offset relative to basepoint
	basepoint = {x=2500,y=0,z=1026},
	hp=10,
	mode = "single",
	protection = "all",
	},
	
	level7 =  {
	startpoint = {x=44,y=30,z=24},   -- start point is an offset relative to basepoint
	basepoint = {x=3800,y=0,z=0},
	protection = "all",
	mode = single,
	hp = 3
	},

	level8 =  {
	startpoint = {x=136,y=70,z=25},   -- start point is an offset relative to basepoint
	basepoint = {x=2800,y=-40,z=2126},
	hp=1,
	protection = "all",
	},	
	
	level9 =  {
	startpoint = {x=30,y=39,z=67},   -- start point is an offset relative to basepoint
	basepoint = {x=3200,y=0,z=0},
--	protection = "all",
	},	
	
}
